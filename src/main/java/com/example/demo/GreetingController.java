package com.example.demo;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.*;

@RestController
public class GreetingController {
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    private ArrayList list = new ArrayList();

    @RequestMapping("/hello")
    public String sayHello(){

        return "Hello Spring";
    }

    @RequestMapping("/test")
    public String sayTest(){
        return "Test String";
    }

    @GetMapping("/greetingSample")
    public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name,
                             @RequestParam(value = "age", defaultValue = "18") int age){
        return new Greeting(counter.incrementAndGet(), String.format(template,name), age);
    }


    @PostMapping("/greeting")
    public String CreateTest(@RequestBody Map<String, String> map){
        list.add(new Greeting(counter.incrementAndGet(),map.get("name"), Integer.parseInt(map.get("age"))){});
        return "Create successful";
    }

    @GetMapping("/greeting")
    public ArrayList GetList(){

        return list;
    }

    @GetMapping("/greetingId")
    public Greeting GetListById(@RequestParam(value = "id", defaultValue = "1") long id){

        for (var item:list) {
            if (((Greeting)item).getId()==(id)) {
                return ((Greeting) item);
            }
        }

        return null;
    }

    @PutMapping("/greeting")
    public String UpdateTest(@RequestParam(value = "id", defaultValue = "1") long id,
                             @RequestBody Map<String, String> map){

        for (var item:list) {
            if (((Greeting) item).getId()==(id)) {
                ((Greeting) item).setName(map.get("name"));
                ((Greeting) item).setAge(Integer.parseInt(map.get("age")));
            }
        }

        return "Update successful";
    }
}
