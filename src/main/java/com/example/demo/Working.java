package com.example.demo;

public class Working {

    private final long id;

    private String message;
    private boolean isFinish;

    public Working(long id, String message, boolean isFinish){
        this.id=id;
        this.message=message;
        this.isFinish =isFinish;
    }

    public long getId(){
        return this.id;
    }

    public String getMessage(){
        return this.message;
    }

    public boolean getIsFinished(){
        return this.isFinish;
    }

    public void setMessage(String message){
        this.message=message;
    }

    public void setIsFinished(boolean isFinish){
        this.isFinish=isFinish;
    }
}
