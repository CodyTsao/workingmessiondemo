package com.example.demo;

import org.springframework.web.bind.annotation.*;

import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
public class WorkingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    private Connection connection;
    private  Statement statement;

    private ArrayList<Working> list = new ArrayList();

    public WorkingController(){
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/new_testdb","root","CfjciB$aVdW5");
            statement = connection.createStatement();

        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    @PostMapping("/working")
    public boolean createNewWork(@RequestBody Map<String, String> map){

        boolean isok=true;

        try{
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO `new_testdb`.`working` (`id`, `message`, `isFinished`) VALUES (?, ?, ?);");

            SimpleDateFormat formatter = new SimpleDateFormat("MMddHHmmss");

            preparedStatement.setLong(1, Long.parseLong(formatter.format(new Date())));
            preparedStatement.setString(2, map.get("message"));
            preparedStatement.setString(3, "False");
            preparedStatement.executeUpdate();

        }catch (Exception ex){
            System.out.println("Failed" + ex.getMessage());
            isok=false;
        }

        return isok;
    }

    @GetMapping("/working")
    public ArrayList getWorkingList(){

        list = new ArrayList();
        try{
            try(ResultSet result = statement.executeQuery("SELECT * FROM new_testdb.working")){
                while (result.next()){
                    list.add(new Working(Long.parseLong(result.getString("id")),result.getString("message"), Boolean.parseBoolean(result.getString("isFinished"))));
                }
            }
        }catch (Exception ex){

        }

        return list;
    }

    @GetMapping("/workingId")
    public Working getWorkingListById(@RequestParam(value = "id", defaultValue = "-1") long id){

        list = new ArrayList();
        try{
            try(ResultSet result = statement.executeQuery("SELECT * FROM new_testdb.working")){
                while (result.next()){
                    list.add(new Working(Long.parseLong(result.getString("id")),result.getString("message"), Boolean.parseBoolean(result.getString("isFinished"))));
                }
            }
        }catch (Exception ex){

        }

        for (var item:list) {
            if (item.getId()==(id)) {
                return (item);
            }
        }

        return null;
    }

    @PutMapping("/working")
    public boolean modifyWorkingStatus(@RequestParam(value = "id", defaultValue = "-1") long id,
            @RequestBody Map<String, String> map){

        boolean isok=true;

        try{

            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE `new_testdb`.`working` SET `isFinished` = ? WHERE (`id` = ?);");

            preparedStatement.setString(1, map.get("isFinished"));
            preparedStatement.setLong(2, id);
            preparedStatement.executeUpdate();

        }catch (Exception ex){
            isok=false;
        }

        return isok;
    }

    @DeleteMapping("/working")
    public boolean deleteWorking(@RequestParam(value = "id", defaultValue = "-1") long id){

        boolean isok=true;

        try{
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM `new_testdb`.`working` WHERE `id` = ?;");

            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();

        }catch (Exception ex){
            isok=false;
        }

        return isok;
    }

}
