package com.example.demo;

public class Greeting {

    private final long id;

    private String name;
    private int age;

    public Greeting(long id, String name, int age){
        this.id=id;
        this.name=name;
        this.age=age;
    }


    public long getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public int getAge(){
        return age;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setAge(int age){
        this.age = age;
    }
}
